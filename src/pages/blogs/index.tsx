import * as React from "react";
import Layout from "../../components/layout/layout";
import BlogList from "../../components/blogs/blogList";
import HelmetSeo from "../../components/seo/helmet";

const BlogPage = () => {
  return (
    <Layout pageTitle="Latest blogs">
      <HelmetSeo
        data={{
          canonical: "",
          description:
            "Reactpool is a community for react to collaborate and work together",
          title: "Reactpool | Blogs",
          index: true,
        }}
      />
      <BlogList />
    </Layout>
  );
};

export default BlogPage;
