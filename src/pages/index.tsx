/** @jsx createElement */
import { createElement } from "react";
import styled from "styled-components";
import { palette } from "../theme/palette";
import home from "../images/react.svg";
import Button from "@mui/material/Button";
import HelmetSeo from "../components/seo/helmet";
import Layout from "../components/layout/layout";
import Grid from "@mui/material/Grid";
import Zoom from "@mui/material/Zoom";

const LandingStyle = styled.div`
  text-align: center;
  margin-top: 70px;
  min-height: 600px;

  p {
    max-width: 400px;
  }

  @media only screen and (max-width: ${palette.breakpoint}) {
    h3 {
      max-width: 250px;
    }
  }
`;

const Hero = styled.img`
  max-width: 100%;
  width: 500px;

  @media only screen and (max-width: ${palette.breakpoint}) {
    width: 250px;
  }
`;

const HeroDetail = styled.div`
  text-align: left;
`;

function Home() {
  return (
    <Layout pageTitle="">
      <LandingStyle>
        <HelmetSeo
          data={{
            canonical: "",
            description: "Michael James is a software development consultant",
            title: "Michael James | Consultant",
            index: true,
          }}
        />

        <Grid container spacing={2}>
          <Grid item xs={12} md={5}>
            <HeroDetail>
              <Zoom in={true} style={{ transitionDelay: "100ms" }}>
                <h1>Hi, I'm Michael.</h1>
              </Zoom>
              <Zoom in={true} style={{ transitionDelay: "200ms" }}>
                <h3>Software engineer. Project consultant.</h3>
              </Zoom>
              <p>
                Experienced software developer specialising in website
                development, UX and project management.
              </p>
              <Button variant="contained">HIRE ME</Button>
            </HeroDetail>
          </Grid>
          <Grid item xs={12} md={7}>
            <Hero src={home} alt="" />
          </Grid>
        </Grid>
      </LandingStyle>
    </Layout>
  );
}

export default Home;
