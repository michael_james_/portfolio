/** @jsx createElement */
import { createElement, useEffect, useState } from "react";
import styled from "styled-components";
import { pushIn } from "../../theme/animations";

export type BaseToastProps = {
  text?: string;
  className?: string;
};

export const BaseToast = ({ text, className }: BaseToastProps) => {
  const [show, setShow] = useState(true);

  useEffect(() => {
    setTimeout(() => {
      setShow(false);
    }, 3000);
  }, []);

  return show ? (
    <ToastComponent className={className}>{text}</ToastComponent>
  ) : null;
};

const ToastComponent = styled.div`
  position: fixed;
  top: 10px;
  left: 0;
  right: 0;
  width: 90%;
  max-width: 400px;
  height: 55px;
  background: red;
  margin: auto;
  display: flex;
  align-items: center;
  border-radius: 8px;
  justify-content: center;
  border-radius: 2px;
  z-index: 2;
  box-shadow: rgb(0 0 0 / 20%) 0px 3px 5px -1px,
    rgb(0 0 0 / 14%) 0px 6px 10px 0px, rgb(0 0 0 / 12%) 0px 1px 18px 0px;
  animation-duration: 0.5s;
  animation-name: ${pushIn};
  animation-iteration-count: initial;
`;
