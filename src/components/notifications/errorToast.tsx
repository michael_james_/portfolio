/** @jsx createElement */
import { createElement } from "react";
import { FunctionComponent } from "react";
import styled, { css } from "styled-components";
import { BaseToast, BaseToastProps } from "./toast";

const ErrorToast = styled<FunctionComponent<BaseToastProps>>(
  (props: BaseToastProps) => <BaseToast {...props} />
)`
  ${() => css`
    && {
      background: #e14040;
      color: #fff;
    }
  `}
`;

export default ErrorToast;
