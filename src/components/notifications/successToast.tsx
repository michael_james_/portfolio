/** @jsx createElement */
import { createElement } from "react";
import { FunctionComponent } from "react";
import styled, { css } from "styled-components";
import { BaseToast, BaseToastProps } from "./toast";

const SuccessToast = styled<FunctionComponent<BaseToastProps>>(
  (props: BaseToastProps) => <BaseToast {...props} />
)`
  ${() => css`
    && {
      background: #04844b;
      color: #fff;
    }
  `}
`;

export default SuccessToast;
