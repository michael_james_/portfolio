import React from "react";
import { Helmet } from "react-helmet";

export interface SiteMeta {
  title: string;
  description: string;
  canonical: string;
  index?: boolean;
}

interface Props {
  data: SiteMeta;
}

const HelmetSeo = (props: Props) => {
  const { data } = props;

  if (!data) {
    return null;
  }

  return (
    <Helmet>
      <meta charSet="utf-8" />
      <title>{data.title}</title>
      <meta
        name="description"
        property="og:description"
        content={data.description}
      />
      <meta name="title" property="og:title" content={data.title} />
      <link rel="canonical" href={data.canonical} />
      {data.index && <meta name="robots" content="follow, index"></meta>}
    </Helmet>
  );
};

export default HelmetSeo;
