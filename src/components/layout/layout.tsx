import Header from "../navigation/header";
import styled from "styled-components";
import React from "react";
import Footer from "../navigation/footer";

const Layout = ({ pageTitle, children }) => {
  return (
    <Wrap>
      <Header />
      {pageTitle !== "" && <h1>{pageTitle}</h1>}
      {children}
      <Footer />
    </Wrap>
  );
};

const Wrap = styled.div`
  margin: 0 auto;
`;

export default Layout;
