import Header from "../navigation/header";
import styled from "styled-components";
import React from "react";

const BlogLayout = ({ pageTitle, children }) => {
  return (
    <Wrap>
      <Header />
      <h1>{pageTitle}</h1>
      {children}
    </Wrap>
  );
};

const Wrap = styled.div`
  margin: 0 auto;
`;

export default BlogLayout;
