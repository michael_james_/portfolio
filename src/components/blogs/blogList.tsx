import React from "react";
import { Link, graphql, useStaticQuery } from "gatsby";
import styled from "styled-components";
import { formatDate } from "../../services/date/date";
import TextField from "@mui/material/TextField";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemText from "@mui/material/ListItemText";
import ListItemAvatar from "@mui/material/ListItemAvatar";
import Avatar from "@mui/material/Avatar";
import WorkIcon from "@mui/icons-material/Work";

interface BlogItem {
  contentful_id: string;
  createdAt: string;
  shortDescription: string;
  slug: string;
  title: string;
  content: {
    raw: string;
  };
}

const LatestArcticles = styled.div`
  background: #fff;
  margin-bottom: 50px;
`;

const StyledInputWrap = styled.div`
  display: flex;
  text-align: left;
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 16px;
  padding-right: 16px;
  align-items: flex-start;
`;

const BlogList = () => {
  const [searchText, setSearchText] = React.useState<string>("");
  const data = useStaticQuery(graphql`
    query BlogIndexQuery {
      allContentfulAsset {
        edges {
          node {
            id
          }
        }
      }
      allContentfulBlogPost {
        nodes {
          contentful_id
          createdAt
          title
          slug
          content {
            raw
          }
          shortDescription
        }
      }
    }
  `);

  const filteredArcticles = (): BlogItem[] => {
    const items: BlogItem[] = data.allContentfulBlogPost.nodes;
    const x = items.filter(
      (x) =>
        x.title.toLocaleLowerCase().indexOf(searchText.toLocaleLowerCase()) > -1
    );

    return x;
  };

  const toRender = filteredArcticles();

  const handleChange = (event: {
    target: { value: React.SetStateAction<string> };
  }) => {
    setSearchText(event.target.value);
  };

  return (
    <LatestArcticles>
      <h1>Blogs</h1>

      <StyledInputWrap>
        <TextField
          id="standard-basic"
          label="Search"
          variant="standard"
          value={searchText}
          onChange={handleChange}
        />
      </StyledInputWrap>
      <List sx={{ width: "100%", maxWidth: 360, bgcolor: "background.paper" }}>
        {toRender.map((node: BlogItem) => (
          <Link key={node.createdAt} to={`/blogs/${node.slug}`}>
            <ListItem>
              <ListItemAvatar>
                <Avatar>
                  <WorkIcon />
                </Avatar>
              </ListItemAvatar>
              <ListItemText
                primary={node.title}
                secondary={formatDate(node.createdAt)}
              />
            </ListItem>
          </Link>
        ))}
      </List>
    </LatestArcticles>
  );
};

export default BlogList;
