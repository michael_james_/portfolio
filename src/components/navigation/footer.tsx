/** @jsx createElement */
import { createElement } from "react";
import styled from "styled-components";
import Divider from "@mui/material/Divider";
import Stack from "@mui/material/Stack";
import linkedin from "../../images/linkedin.svg";
import facebook from "../../images/facebook.svg";
import email from "../../images/email.svg";
import Grid from "@mui/material/Grid";
import { palette } from "../../theme/palette";

const Wrap = styled.div`
  margin-top: 200px;
  padding-bottom: 100px;
`;
const FooterStyle = styled.div`
  padding-top: 20px;

  a {
    text-align: left;
    justify-content: flex-start;
  }
`;

const FooterMuted = styled.p`
  color: gray;
  font-size: 14px;
  margin-bottom: 0px;
`;

const FooterMutedSm = styled(FooterMuted)`
  font-size: 12px;
`;

const Social = styled.img`
  width: 25px;
  opacity: 0.7;
`;

const SocialRow = styled(Stack)`
  justify-content: end;

  @media only screen and (max-width: ${palette.tabletbreakpoint}) {
    justify-content: start;
  }
`;
const DetailRow = styled(Stack)`
  justify-content: start;
`;

function Footer() {
  return (
    <Wrap>
      <Divider />
      <FooterStyle>
        <Grid container spacing={2}>
          <Grid item xs={12} md={6}>
            <DetailRow spacing={1} direction="column">
              <FooterMuted>Copyright @Michael James 2022</FooterMuted>
              <FooterMutedSm>michaelcjames10@gmail.com</FooterMutedSm>
            </DetailRow>
          </Grid>
          <Grid item xs={12} md={6}>
            <SocialRow spacing={1} direction="row">
              <a href="https://www.linkedin.com/in/michael-james-12b49968/">
                <Social src={linkedin} alt="linkedin" />
              </a>
              <a href="https://www.linkedin.com/in/michael-james-12b49968/">
                <Social src={facebook} alt="facebook" />
              </a>
              <a href="https://www.linkedin.com/in/michael-james-12b49968/">
                <Social src={email} alt="email" />
              </a>
            </SocialRow>
          </Grid>
        </Grid>
      </FooterStyle>
    </Wrap>
  );
}

export default Footer;
