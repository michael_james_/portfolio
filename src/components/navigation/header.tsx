/** @jsx createElement */
import { createElement, Fragment } from "react";
import styled from "styled-components";
import linkedin from "../../images/linkedin.svg";
import facebook from "../../images/facebook.svg";
import email from "../../images/email.svg";
import Zoom from "@mui/material/Zoom";

const HeaderStyle = styled.header`
  display: flex;
  align-items: center;
  justify-content: center;
  padding-top: 20px;

  img {
    width: 40px;
  }

  p:last-of-type {
    margin-left: 10px;
  }
`;

function Header() {
  return (
    <Fragment>
      <HeaderStyle>
        <Zoom in={true} style={{ transitionDelay: "500ms" }}>
          <a href="https://www.linkedin.com/in/michael-james-12b49968/">
            <img src={linkedin} alt="linkedin" />
          </a>
        </Zoom>
        <Zoom in={true} style={{ transitionDelay: "600ms" }}>
          <a href="https://www.linkedin.com/in/michael-james-12b49968/">
            <img src={facebook} alt="facebook" />
          </a>
        </Zoom>
        <Zoom in={true} style={{ transitionDelay: "700ms" }}>
          <a href="https://www.linkedin.com/in/michael-james-12b49968/">
            <img src={email} alt="email" />
          </a>
        </Zoom>
      </HeaderStyle>
    </Fragment>
  );
}

export default Header;
