export enum AppPage {
  Home = "/",
  About = "/about",
}

export enum Title {
  H1 = "h1",
  H2 = "h2",
  H3 = "h3",
  P = "p",
  PTitle = "ptitle",
  PMuted = "pmuted",
}

export enum TextAlign {
  Left = "left",
  Center = "center",
  Right = "right",
}

export enum Language {
  EN = "en",
  IE = "ie",
}
