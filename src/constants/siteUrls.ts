export interface SiteUrl {
  slug: string;
  display: string;
  badge?: string;
}

export const siteUrls: SiteUrl[] = [
  {
    display: "Home",
    slug: "/",
  },
  {
    display: "Blogs",
    slug: "/blogs",
  },
  {
    display: "About",
    slug: "/about",
  },
];

export enum SitePaths {
  Home = "/",
  Blogs = "/blogs",
  About = "/about",
}
