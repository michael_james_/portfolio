export interface UserProfile {
  detail?: string;
  title?: string;
  user?: { email: string; recruiter: boolean };
  email?: string;
  userId?: string;
  result?: string;
}
