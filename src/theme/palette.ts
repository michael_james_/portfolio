export const palette: Palette = {
  main: "#e91e63",
  second: "#63f9d6",
  darkmain: "#191919",
  textmuted: "#747474",
  breakpoint: "666px",
  tabletbreakpoint: "900px",
  skeleton: "linear-gradient(to right, rgb(236, 233, 230), rgb(255, 255, 255))",
};

export interface Palette {
  main: string;
  second: string;
  darkmain: string;
  textmuted: string;
  breakpoint: string;
  tabletbreakpoint: string;
  skeleton: string;
}
