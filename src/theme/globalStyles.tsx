import { createGlobalStyle } from "styled-components";
import { palette } from "./palette";

export default createGlobalStyle`

@font-face {
    font-family: 'Roboto';
    font-style: normal;
    font-weight: 700;
    src: local('Roboto'), url('https://fonts.cdnfonts.com/s/12165/Roboto-Regular.woff') format('woff');
    font-display: swap;
}

@font-face {
    font-family: 'Roboto Medium';
    font-style: normal;
    font-weight: 500;
    src: local('Roboto Medium'), url('https://fonts.cdnfonts.com/s/12165/Roboto-Medium.woff') format('woff');
    font-display: swap;
}

@font-face {
    font-family: 'Roboto-Bold';
    font-style: normal;
    font-weight: 500;
    src: local('Roboto Medium'), url('https://fonts.cdnfonts.com/s/12165/Roboto-Black.woff') format('woff');
    font-display: swap;
}

html{
    overflow-x: hidden;
  }
  body {
    font-family:Roboto;
    overflow: hidden;
    padding: 0 5%;
    max-width: 1300px;
    margin: 0px auto;
    font-display: swap;
  }

  a{
    text-decoration: none;
    color: ${palette.darkmain};
  }
  h1,h2,h3,h4,h5{
      font-family: Roboto-Bold;
    }

    h1{
      font-size: 3.5em;
      margin: 0px;
      max-width: 760px;
    }

    p{
      font-size: 18px;
    }
`;
