const React = require("react");
const GlobalStyles = require("./src/theme/globalStyles").default;

exports.onRenderBody = ({ setHtmlAttributes }) => {
  setHtmlAttributes({
    lang: "en",
  });
};

exports.wrapPageElement = ({ element, props }) => {
  return (
    <span>
      <GlobalStyles />
      {element}
    </span>
  );
};
